const webpack = require('webpack');
const WebpackNotifierPlugin = require('webpack-notifier');
const path = require('path');
const config = require('./webpack.config');

config.mode = 'development';
config.devtool = 'eval-source-map';
config.devServer = {
    inline: true,
    hot: true,
    host: 'localhost',
    port: 3030,
    contentBase: path.join(__dirname, "public"),
};
config.plugins = config.plugins.concat([
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('development'),
        },
    }),
    new WebpackNotifierPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
]);
config.output.pathinfo = true;
config.module.rules = config.module.rules.concat([
    {
        test: /\.(sass|scss)/,
        exclude: /node_modules/,
        use: [
            {
                loader: "style-loader"
            },
            {
                loader: "css-loader",
                options: {
                    sourceMap: true,
                }
            },
            {
                loader: "resolve-url-loader",
            },
            {
                loader: "sass-loader",
                options: {
                    sourceMap: true,
                }
            }]
    }
])

module.exports = config;
