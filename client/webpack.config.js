const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: [
        './src/app.js',
    ],
    output: {
        path: path.join(__dirname, './public/static/build/'),
        filename: 'main.js',
    },
    devServer: {
        contentBase: path.join(__dirname, "public"),
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader"
                    }
                ]
            },
            {test: /\.jpg$/, use: 'url?limit=100000&mimetype=image/jpg'},
            {test: /\.png$/, use: 'url?limit=50000&mimetype=image/png'},
            {test: /\.svg/, use: 'url?limit=50000&mimetype=image/svg+xml'},
            {test: /\.(woff|woff2|ttf|eot)/, use: 'url?limit=500000'},
            {
                test: /\.gif$/,
                // use: 'url?limit=16000&mimetype=image/gif'
                use: 'url'
            },
            {
                test: /\.jsx$/,
                use: {
                    loader: 'babel-loader'
                },
                exclude: [/node_modules/, /public/]
            },
            {
                test: /\.js$/,
                use: {
                    loader: "babel-loader"
                },
                exclude: [/node_modules/, /public/]
            },
            {
                test: /\.json$/, use: {
                    loader: 'json'
                }
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: './index.html',
            template: './src/index.html',
            inject: 'body',
            hash: true,
        }),
    ],
};

