import React from 'react';
import Header from "../modules/header/component/index.jsx";
import {Route, Switch} from "react-router-dom";
import Home from "../modules/home/component/index.jsx";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import CarList from "../modules/car/components/carList.jsx";
import CarItem from "../modules/car/components/carItem.jsx";
import Error from "../modules/common/components/error.jsx";

class App extends React.Component {
    render() {
        return (
            <div className={'main'}>
                <Header/>
                <div className="container">
                    <Error/>
                    <Switch>
                        <Route exact path='/' component={Home}/>
                        <Route path='/cars' component={CarList}/>
                        <Route path='/car/:id' component={CarItem}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default App;
