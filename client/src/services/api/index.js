import Client from './client';
import config from "../../config";

import Car from './car';

function apiFactory({baseURL} = {}) {
    const api = new Client({baseURL});

    return {
        car: new Car({client: api}),
    };
}

export default apiFactory({baseURL: config.API_URL});
