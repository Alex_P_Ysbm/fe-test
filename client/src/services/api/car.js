import Base from './Base';

class Car extends Base {
    getItem({id}) {
        const url = `/cars/${id}`;
        return this.client.get(url);
    }

    getList() {
        const url = '/cars';
        return this.client.get(url);
    }
}

export default Car;
