import React from "react";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import selectors from "../../common/selectors";

class Header extends React.Component {
    render() {

        const {isBusy} = this.props;

        return (
            <div>
                <header>
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <div className="container d-flex justify-content-between">
                            <Link className="navbar-brand" to="/">FE-Test</Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto">
                                    {/*<li className="nav-item active">*/}
                                    {/*<Link className="nav-link" to="/">Home</Link>*/}
                                    {/*</li>*/}
                                    <li className="nav-item active">
                                        <Link className="nav-link" to="/cars">Cars</Link>
                                    </li>
                                </ul>
                            </div>
                            <img src="/assets/image/ajax-loader.gif" width="30" height="30" className={isBusy ? 'd-inline-block align-top' : 'invisible'} alt=""/>
                        </div>
                    </nav>
                </header>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    isBusy: selectors.generalIsBusySelector(state)
});

export default connect(mapStateToProps)(Header);
