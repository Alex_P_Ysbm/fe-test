import types from '../actions/types';

export default (state = {}, action) => {
    switch (action.type) {
        case types.SET_BUSY:
            return setBusy(state, action);
        case types.SET_ERROR:
            return setError(state, action);
        case types.SET_GENERAL_ERROR:
            return setGeneralError(state, action);
        default:
            return state;
    }
};

function setBusy(state, action) {
    return {
        ...state,
        [action.payload.key]: {
            ...state[action.payload.key],
            isBusy: action.payload.value,
        },
    };
}

function setError(state, action) {
    return {
        ...state,
        [action.error.key]: {
            ...state[action.error.key],
            error: action.error.value,
        },
    };
}

function setGeneralError(state, action) {
    return {
        ...state,
        generalError: action.payload,
    };
}
