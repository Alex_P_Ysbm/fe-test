import {createSelector} from "reselect";
import _ from 'lodash';

const commonStateSelector = state => state.common

export default {
    generalIsBusySelector: createSelector(
        commonStateSelector,
        (commonState) => {
            return !!_.filter(commonState, (item) => {
                return item && item.isBusy
            }).length;
        }
    )
}


