import types from './types';

export default {
    setBusy(key, value = true) {
        return {
            type: types.SET_BUSY,
            payload: {key, value},
        };
    },
    setGeneralError(value) {
        return {
            type: types.SET_GENERAL_ERROR,
            payload: value,
        };
    },
    success(type, payload) {
        return (dispatch) => {
            dispatch(this.setBusy(type, false));
            const action = payload ? {type, payload} : {type};
            dispatch(action);
        };
    },
    failure(key, error) {
        return (dispatch) => {
            dispatch(this.setBusy(key, false));
            dispatch(this.setGeneralError(error));
            const action = {
                type: types.SET_ERROR,
                error: {
                    key,
                    value: {
                        ...error,
                    },
                },
            };
            dispatch(action);
        };
    },
}
