import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import actions from '../actions';

class Error extends React.Component {

    handleCloseClick = () => {
        this.props.setGeneralError('');
    }

    render() {
        const {error} = this.props;
        return (
            <div className="row mt-3">
                {error &&
                <div className="alert alert-danger alert-dismissible w-100" role="alert">
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.handleCloseClick}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {error}
                </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    error: state.common.generalError
});

const mapDispatchToProps = (dispatch) => ({
    setGeneralError: bindActionCreators(actions.setGeneralError, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Error);
