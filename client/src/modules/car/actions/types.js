export default {
    FETCH_CARS: 'FETCH_CARS',
    FETCH_CAR: 'FETCH_CAR',
    CLEAR_DATA_STATE: 'clear_data_state',
};
