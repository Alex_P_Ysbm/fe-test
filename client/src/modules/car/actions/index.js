import types from './types';
import commonActions from '../../common/actions';
import api from '../../../services/api';

export const getCars = () => (dispatch) => {
    dispatch(commonActions.setBusy(types.FETCH_CARS));
    return api.car.getList().then(
        (data) => {
            dispatch(commonActions.success(types.FETCH_CARS, data));
        },
        (error) => {
            dispatch(commonActions.failure(types.FETCH_CARS, error));
        }
    );
};

export const getCar = (params) => (dispatch) => {
    dispatch(commonActions.setBusy(types.FETCH_CAR));
    return api.car.getItem(params).then(
        (data) => {
            dispatch(commonActions.success(types.FETCH_CAR, data));
        },
        (error) => {
            dispatch(commonActions.failure(types.FETCH_CAR, error));
        }
    );
};

export const clearDataState = () => ({
    type: types.CLEAR_DATA_STATE,
});
