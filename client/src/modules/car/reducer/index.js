import types from '../actions/types';

const DEFAULT_STATE = {
    car: {},
    cars: [],
};

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case types.FETCH_CAR:
            return getCar(state, action);
        case types.FETCH_CARS:
            return getCars(state, action);
        case types.CLEAR_DATA_STATE:
            return clearDataState(state, action);
        default:
            return state;
    }
};

function getCar(state, action) {
    return {
        ...state,
        car: action.payload.data,
    };
}

function getCars(state, action) {
    return {
        ...state,
        cars: action.payload.data,
    };
}

function clearDataState() {
    return {
        ...DEFAULT_STATE,
    };
}
