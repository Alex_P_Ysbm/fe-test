import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from "react-redux"
import * as actions from '../actions';
import {bindActionCreators} from "redux"

class CarItem extends React.Component {

    componentDidMount() {
        this.props.getCar({id: this.props.match.params.id});
    }

    componentWillUnmount() {
        this.props.clearDataState();
    }

    render() {
        const {car} = this.props;
        return (
            <div>
                <div className="row">
                    {car &&
                    <div className="col-md-12">
                        <div className="card flex-md-row mb-4 box-shadow h-md-250">
                            <img className="card-img-left my-auto" alt={car.title} src={car.image}/>
                            <div className="card-body d-flex flex-column align-items-start">
                                <strong className="d-inline-block mb-2 text-primary">{car.price}</strong>
                                <h3 className="mb-0">{car.title} {car.year}</h3>
                                <div className="mb-1 text-muted">{car.announce_date}</div>
                                <p>{car.body_type}</p>
                                <p>{car.engine}</p>
                                <p>{car.gearbox}</p>
                                <p>{car.region}</p>
                                <p className="card-text mb-auto">{car.description}</p>
                            </div>
                        </div>
                    </div>
                    }
                </div>
                <div className="row float-right">
                    <button type="button" className="btn btn-link">
                        <Link to="/cars">&lt; Back</Link>
                    </button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    car: state.garage.car,
});

const mapDispatchToProps = (dispatch) => ({
    getCar: bindActionCreators(actions.getCar, dispatch),
    clearDataState: bindActionCreators(actions.clearDataState, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CarItem);
