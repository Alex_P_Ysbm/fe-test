import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from "react-redux"
import * as actions from '../actions';
import {bindActionCreators} from "redux"

class CarList extends React.Component {

    componentDidMount() {
        this.props.getCars();
    }

    renderItems(cars) {
        return cars.map((item, idx) =>
            (
                <tr key={item.id}>
                    <td scope="row">
                        <Link className="" to={`/car/${item.id}`}>{item.id}</Link>
                    </td>
                    <td>
                        <img src={item.image} className="rounded float-left" alt={item.title}/>
                    </td>
                    <td>{item.title}</td>
                    <td>{item.price}</td>
                    <td>{item.year}</td>
                </tr>
            )
        )
    }

    render() {

        const {cars} = this.props;

        return (
            <table className="table table-striped">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Year</th>
                </tr>
                </thead>
                <tbody>

                {cars && this.renderItems(cars)}

                </tbody>
            </table>
        )
    }
}

const mapStateToProps = (state) => ({
    cars: state.garage.cars,
});

const mapDispatchToProps = (dispatch) => ({
    getCars: bindActionCreators(actions.getCars, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CarList);
