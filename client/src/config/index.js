import merge from 'lodash/merge';
import global from './global';

let env = (process.env.NODE_ENV === 'development') ? require('../../env.development') : require('../../env.production');

export default merge(global, env);