import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux'

import common from '../modules/common/reducer';
import garage from '../modules/car/reducer';

const rootReducer = combineReducers({
    router: routerReducer,
    common,
    garage
});

export default rootReducer;
