import {createStore, applyMiddleware} from 'redux';
import {routerMiddleware} from "react-router-redux"
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from './root.reducer';
import createHistory from 'history/createBrowserHistory';

const loggerMiddleware = createLogger();
const history = createHistory();

const createStoreWithMiddleware = applyMiddleware(
    thunkMiddleware,
    loggerMiddleware,
    routerMiddleware(history)
)(createStore);

function configureStore(initialState) {
    const store = createStoreWithMiddleware(
        rootReducer,
        initialState,
        window.devToolsExtension && window.devToolsExtension()
    );
    if (module.hot) {
        module.hot.accept('./root.reducer.js', () => {
            store.replaceReducer(rootReducer);
        });
    }
    return store;
}

export {configureStore, history}