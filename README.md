# FE-test

Installation.

Run `yarn install` under root directory.

Run `yarn install` under `client` directory.

Rename `example.env.local.json` as `env.local.json` (optional).

Rename `/client/example.env.development` as `env.development` (required).

To start server run `npm run start` under root directory.

To start client run `npm run develop:devserver` under `client` directory.

Visit http://localhost:3030/