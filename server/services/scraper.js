import got from 'got';
import cheerio from "cheerio";
import iconv from "iconv-lite";
import config from "../config";

export default {

    /**
     * Get loaded cheerio
     *
     * @param url
     * @returns {Promise<*>}
     */
    get(url) {
        return new Promise((res, rej) => {
            got.stream(url).pipe(iconv.decodeStream(config.app.scraper.encoding)).collect((err, decodedBody) => {
                if (err) return rej(err);
                return res(cheerio.load(decodedBody));
            });
        })
    }
}
