import NodeCache from "node-cache";

const cache = new NodeCache({
    stdTTL: 360
});

export default {
    set(key, value) {
        return cache.set(key, value);
    },
    get(key) {
        return new Promise((res, rej) => {
            cache.get(key, (err, value) => {
                if (err) return rej(err);
                return res(value);
            })
        })
    }
}