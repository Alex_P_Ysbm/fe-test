import store from "../../../services/cache";
import _ from "lodash";

const KEY_LIST = 'CARS';

export default {
    async get() {
        return await store.get(KEY_LIST);
    },
    async getById(id) {
        const list = await store.get(KEY_LIST);
        return _.find(list, {id});
    },
    save(list) {
        return store.set(KEY_LIST, list);
    }
}