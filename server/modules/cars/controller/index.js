import service from '../service';
import config from "../../../config";
import scraper from "../../../services/scraper";
import storage from "../storage";
import AppError from "../../../helpers/error/appError"

export default {
    async getList(req, res, next) {

        /**
         * Get from store
         */
        let data = await storage.get();

        if (data) {
            return res.json({data});
        }

        /**
         * Grab list
         */
        const $ = await scraper.get(config.app.scraper.root_url);

        /**
         * Parse list
         */
        data = service.parseList($);

        /**
         * Save list
         */
        storage.save(data);

        res.json({data});
    },
    async getItem(req, res) {

        const car = await storage.getById(req.params.id);

        if (!car) {
            throw new AppError({status: 422, message: 'Selected car is not found. Please refresh the list.'})
        }

        /**
         * Grab item
         */
        const $ = await scraper.get(car.url);

        /**
         * Parse item
         */
        const data = service.parseItem($);

        res.json({data});
    },
};
