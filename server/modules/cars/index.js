import express from 'express';
import Controller from './controller';
import wrapAsync from "../../helpers/wrapAsync"

const routes = express.Router();

routes
    .get('/cars', wrapAsync(Controller.getList))
    .get('/cars/:id', wrapAsync(Controller.getItem))

export default routes;
