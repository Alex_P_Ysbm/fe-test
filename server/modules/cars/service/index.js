import config from '../../../config';

export default {
    parseList($) {
        return $('div[id^="rst-ocid-"]').map((idx, item) => {
            const description = $(item).find('.rst-ocb-i-d-l-i-s');
            return {
                id: $(item).attr('id').match(/\d+$/)[0],
                url: config.app.scraper.host + $(item).find('.rst-ocb-i-a').attr('href'),
                title: $(item).find('.rst-ocb-i-h').text(),
                image: $(item).find('.rst-ocb-i-i').attr('src'),
                price: description.eq(0).text(),
                year: description.eq(2).text(),
            }
        }).get();
    },
    parseItem($) {
        const mainSection = $('div[id="rst-page-left-column"]');
        mainSection.find('#rst-page-oldcars-item-header').children().first().remove();
        const tableSectionRows = mainSection.find(".rst-uix-table-superline tr");
        return {
            image: mainSection.find('#rst-page-oldcars-mainphoto').attr('src'),
            title: mainSection.find('#rst-page-oldcars-item-header').text().trim(),
            description: mainSection.find('#rst-page-oldcars-item-option-block-container-desc').text().trim(),
            price: tableSectionRows.eq(0).find('.rst-uix-price-param').children().first().text().trim(),
            year: tableSectionRows.eq(1).find('.rst-uix-black').text().trim(),
            engine: tableSectionRows.eq(2).find('td').last().text().trim(),
            gearbox: tableSectionRows.eq(3).find('td').last().text().trim(),
            body_type: tableSectionRows.eq(4).find('td').last().text().trim(),
            region: tableSectionRows.eq(5).find('td').last().text().trim(),
            announce_date: tableSectionRows.eq(7).find('td').last().text().trim(),
        }
    }

}