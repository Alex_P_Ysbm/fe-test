import express from 'express';
import cars from './cars';

const routes = express.Router();

routes.use(cars);

export default routes;
