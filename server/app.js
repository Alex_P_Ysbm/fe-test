import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';

import config from './config';
import modules from './modules';
import error from './helpers/error';

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/api', modules);
app.use(error);

const port = config.system.port;
app.listen(port);
console.log('Server started on port: ' + port);

export default app;
