import config from '../../config';

export default (err, req, res, next) => {

    const {status = 500, message = 'Server Error', data = {}, name = 'Error'} = err;

    switch (name) {
        default:
            res.status(status).json({error: {message, data}});
    }

    if (config.env === 'development') {
        console.error(err);
    }
};
