export default function AppError({status, message, data = {}, name = 'AppError'}) {
    Error.call(this);
    Error.captureStackTrace(this);

    this.message = message;
    this.status = status;
    this.data = data;
    this.name = name;
}
