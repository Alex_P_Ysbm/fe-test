import nconf from 'nconf';
import path from 'path';

nconf
    .argv()
    .env({
        transform(object) {
            const obj = object;
            if (typeof obj.value !== 'string') {
                return false;
            }
            obj.value = obj.value.trim();
            return obj;
        },
    })
    .file({file: path.join(__dirname, '..', '..', '/env.local.json')})
    .defaults({
        env: nconf.get('NODE_ENV') || 'development',
        system: {
            port: nconf.get('NODE_PORT') || 3001,
        },
        app: {
            scraper: {
                host: 'http://rst.ua',
                root_url: 'http://rst.ua/oldcars/audi',
                encoding: 'win1251'
            }
        }
    });

export default nconf.get();
